if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper avw2fsl calc_grad_perc_dev distancemap fsl2ascii fsladd fslascii2img fslcc fslchfiletype fslchfiletype_exe fslchpixdim fslcomplex fslcpgeom fslcreatehd fsledithd fslfft fslhd fslinfo fslinterleave fslmaths fslmeants fslmerge fslmodhd fslnvols fslorient fslpspec fslreorient2std fslroi fslselectvols fslsize fslslice fslsmoothfill fslsplit fslstats fslswapdim fslswapdim_exe fslval
fi
